## **Otto Gori** - Professional Happy Dude  [linkedin](https://www.linkedin.com/in/otto-gori/)  

<!-- blank line -->
----
<!-- blank line -->

> ### Walk of life
>> **SRE / Cloud DevOps Leader at HarlemNext**. (12/20 ~ Today).<br>
My Role: Working side by side with C levels, management and technical leadership, defining the future of IT in this global scale company. Responsible for a cloud footprint listed among worldwide biggest Google Clients<br>
The Business: Harlem Next dives into the world of extreme high traffic web applications. Creating platforms used by millions of unique visitors every day.
>
>> **Google Cloud Architect at BirminD**. (01/20 ~ 05/20).<br>
The Business: Industrial signal collection technology.<br>
My contribution: Cloud transformation project, helped the company migrate their systems to the cloud.
>
>> **Multi Cloud Architect at QuintoApp**. (09/19 ~ 01/20). <br>
The Business: Social network that promotes live discussion and exchange of ideas on hot and controversial topics.<br>
My contribution: Case of technology stack maturation and perfection in a moment of heavy user base growth, leader on architecting the new technology stack.
>
>> **DevOps / Google Cloud Architect at Goomer**. (01/19 ~ 09/19). <br>
The Business: Brazilian market leader on self-service interactive devices for restaurants.<br>
My contribution: Pioneer in CICD procedures, cloud migration and refctoring of systems into microservices architecture.
>
>> **Internet of Things Researcher / DevOps  at Going2 & ZF/Openmatics**. (09/17 ~ 05/19).<br>
The Business: Automobilistic technology research.<br>
My contribution: Researcher | Cloud architecture for a IoT devices signal collection system used on BigData analysis of heavy transport vehicles | Fuel fraud detection | Personal vehicle self diagnose App 
>
>> **Personal Endeavour - Wootz.Software** (01/16 ~ 01/21).<br>
The Business: Started a company with the goal of serving Scaleups and Startups on the challenge of maturing their technology stack and offering a reliable tech leadership role for development, researchers and engineering teams. After moving to The Netherlands in 2020 I sold its ownership to a partner company who continued serving our client base.
>
>> **Product Owner at Concrete Solutions**. (01/17 ~ 07/17). 
>
>> **Software Developer at GFT Technologies**. (05/15 ~ 01/17).  
>
>> **Software Developer on Ge-Fisc SPED**. (2012 ~ 2014).  
>
>> **Blacksmith and custom motorcycle builder at [ottogori.art](https://www.instagram.com/ottogori.art/)**. 

----

>### Academics: 
>>**Computer Scientist Bachelor** (2020). <br>
>>**Technical high school degree** (2009 ~ 2011). <br>
>>**Several papers and publications**. <br>
>>**Strongly active on various developer communities**. <br>

----